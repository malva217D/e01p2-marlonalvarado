package ejerciciodos;


import java.util.ArrayList;
import javax.swing.JOptionPane;



public class Main {

    public static void main(String[] args) {

        EjercicoDos funciones = new EjercicoDos();

        String menu;
        boolean cont = true;
        
        do {
            
            menu = JOptionPane.showInputDialog("Seleccione la Opcion \n"
                    + "1-Generar Arreglo \n"
                    + "2-Multiplicar Pares e Impares \n"
                    + "3-Salir."
            );

            switch (menu) {
                case "1":
                    
                    int num1 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor 1: "));
                    int num2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor 2: "));
                    JOptionPane.showMessageDialog(null, funciones.generarArreglo(num1, num2));
                    break;

                case "2":
                    boolean ingresarMas = true;
                    ArrayList<Integer> arreglo = new ArrayList<>();
                    JOptionPane.showMessageDialog(null, "Cree un Arreglo.");
                    do {
                        int num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor: "));
                        arreglo.add(num);
                        ingresarMas = JOptionPane.showConfirmDialog(null, "Desea Ingresar mas?") == 0;

                    } while (ingresarMas);
                    JOptionPane.showInternalMessageDialog(null, funciones.multiplicarParesImpares(arreglo));
                    break;
                case "3":
                    cont = false;
                    break;
                default:
                    break;

            }

        
        }while (cont);
            
        }
    }


