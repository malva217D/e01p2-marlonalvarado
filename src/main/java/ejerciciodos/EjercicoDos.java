package ejerciciodos;

import java.util.ArrayList;
import javax.swing.JOptionPane;

public class EjercicoDos {

    int num1 = 0;
    int num2 = 0;

    public ArrayList generarArreglo(int num1, int num2) {
        ArrayList<Integer> lista = new ArrayList<>();
        try {
            if (num1 < num2) {
                for (int i = num1; i <= num2; i++) {
                    lista.add(i);

                }

            } else {
                for (int i = num1; i >= num2; i--) {
                    lista.add(i);

                }
            }
            return lista;
        } catch (Exception e) {
            JOptionPane.showInternalMessageDialog(null, e);
            return lista;
        }
    }

    public int multiplicarParesImpares(ArrayList<Integer> arreglo) {
        try {
            int sumaPares = 0;
            int sumaImPares = 0;

            for (Integer num : arreglo) {
                if (num % 2 == 0) {
                    sumaPares += num;

                } else {
                    sumaImPares += num;

                }

            }
            return sumaPares + sumaImPares;
        } catch (Exception e) {
            JOptionPane.showInternalMessageDialog(null, e);
            return 0;
        }

    }

}
